﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Firebase.Database;
using FireSharp;
using FireSharp.Config;
using FireSharp.Interfaces;
using FireSharp.Response;
using Newtonsoft.Json.Linq;

namespace firebasedatabase
{

    public class FirebaseService
    {
        String auth = "dVx8o3Aj6IQRTPzeN9uJcHbW6oR3x2ipKhIJ7IZ9"; // your app secret
        String path = "https://dailyxe-ionic-raovat.firebaseio.com/";// your path database
        Firebase.Database.FirebaseClient firebaseClient;
        IFirebaseClient client;
        public FirebaseService()
        {
            firebaseClient = new Firebase.Database.FirebaseClient(
              path,
              new FirebaseOptions
              {
                  AuthTokenAsyncFactory = () => Task.FromResult(auth)

              });

            IFirebaseConfig config = new FirebaseConfig
            {
                AuthSecret = auth,
                BasePath = path
            };
            client = new FireSharp.FirebaseClient(config);

        }
        //Dạng insert key by đường dẫn
        public virtual async Task<JObject> PostAsync(String resourceName, JObject value)
        {
           SetResponse response = await client.SetAsync(resourceName, value);
            return response.ResultAs<JObject>(); //The response will contain the data written


        }
        //Dạng insert key được tạo auto
        public virtual async Task<JObject> PushAsync(String resourceName, JObject value)
        {
            PushResponse response = await client.PushAsync(resourceName, value);
            return response.ResultAs<JObject>(); //The response will contain the data written


        }
        public virtual async Task<JObject> PutAsync(String resourceName, JObject value)
        {
            FirebaseResponse response = await client.UpdateAsync(resourceName, value);
            return response.ResultAs<JObject>(); //The response will contain the data written


        }
        public virtual async Task<bool> DeleteAsync(String resourceName)
        {
            FirebaseResponse response = await client.DeleteAsync(resourceName);
            return response.StatusCode.ToString() == "OK"; //The response will contain the data written


        }

        public virtual async Task<JObject> GetAsync(String resourceName)
        {
          return  await firebaseClient.Child(resourceName).OnceSingleAsync<JObject>();


        }

    }
}
