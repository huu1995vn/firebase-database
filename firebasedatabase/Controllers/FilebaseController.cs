﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Firebase.Database;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace firebasedatabase.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FirebaseController : ControllerBase
    {
        FirebaseService firbaseservice = new FirebaseService();
        // GET api/values
        [HttpGet]
        public async Task<ActionResult<string>> GetAsync()
        {
            var table = Request.Query["table"].ToString();

            if(string.IsNullOrEmpty(table))
            {
                return null;
            }
            JObject res = await firbaseservice.GetAsync(table);
            if (res.Values() == null) return null;
            return JsonConvert.SerializeObject(res.Values());
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public async Task<ActionResult<string>> Get(int id)
        {
            var table = Request.Query["table"].ToString();

            if (string.IsNullOrEmpty(table))
            {
                return null;
            }
            JObject res = await firbaseservice.GetAsync(table +"/"+ id.ToString());
            if (res == null) return null;

            return JsonConvert.SerializeObject(res);
        }

        // POST api/values
        [HttpPost]
        public async Task<JObject> PostAsync([FromBody] JObject value)
        {
            var table = Request.Query["table"].ToString();

            if (string.IsNullOrEmpty(table))
            {
                return null;
            }

            string id = value["Id"].ToString();
            if(String.IsNullOrEmpty(id))
            {
                return null;
            }
            return await firbaseservice.PostAsync(table + "/" + id, value);
        }
        // POST api/values

        [HttpPost]
        [Route("[action]")]
        public async Task<JObject> PushAsync([FromBody] JObject value)
        {
            var table = Request.Query["table"].ToString();

            if (string.IsNullOrEmpty(table))
            {
                return null;
            }

            string id = value["Id"].ToString();
            if (String.IsNullOrEmpty(id))
            {
                return null;
            }
            return await firbaseservice.PushAsync(table + "/" + id, value);
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public async Task<JObject> Put(int id, [FromBody] JObject value)
        {
            var table = Request.Query["table"].ToString();

            if (string.IsNullOrEmpty(table))
            {
                return null;
            }
            string strId = id.ToString();
            if (String.IsNullOrEmpty(strId))
            {
                return null;
            }
            return await firbaseservice.PutAsync(table + "/" + strId, value);
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public async Task<bool> Delete(int id)
        {
            var table = Request.Query["table"].ToString();

            if (string.IsNullOrEmpty(table))
            {
                return false;
            }
            string strId = id.ToString();
            if (String.IsNullOrEmpty(strId))
            {
                return false;
            }
            return await firbaseservice.DeleteAsync(table + "/" + strId);

        }
    }
}
