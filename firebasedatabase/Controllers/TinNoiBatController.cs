﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Firebase.Database;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace firebasedatabase.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TinNoiBatController : ControllerBase
    {
        FirebaseService firbaseservice = new FirebaseService();
        // GET api/values
        [HttpGet]
        public async Task<ActionResult<string>> GetAsync()
        {
            JObject res = await firbaseservice.GetAsync("dangTinNoiBat");
            return JsonConvert.SerializeObject(res.Values());
            //return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public async Task<ActionResult<string>> Get(int id)
        {
            JObject res = await firbaseservice.GetAsync("dangTinNoiBat/"+ id.ToString());
            return JsonConvert.SerializeObject(res);
        }

        // POST api/values
        [HttpPost]
        public async Task<JObject> PostAsync([FromBody] JObject value)
        {
            string id = value["Id"].ToString();
            if(String.IsNullOrEmpty(id))
            {
                return null;
            }
            return await firbaseservice.PostAsync("dangTinNoiBat/"+ id, value);
        }
        // POST api/values

        [HttpPost]
        [Route("[action]")]
        public async Task<JObject> PushAsync([FromBody] JObject value)
        {
            string id = value["Id"].ToString();
            if (String.IsNullOrEmpty(id))
            {
                return null;
            }
            return await firbaseservice.PushAsync("dangTinNoiBat/" + id, value);
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public async Task<JObject> Put(int id, [FromBody] JObject value)
        {
            string strId = id.ToString();
            if (String.IsNullOrEmpty(strId))
            {
                return null;
            }
            return await firbaseservice.PutAsync("dangTinNoiBat/" + strId, value);
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public async Task<bool> Delete(int id)
        {
            string strId = id.ToString();
            if (String.IsNullOrEmpty(strId))
            {
                return false;
            }
            return await firbaseservice.DeleteAsync("dangTinNoiBat/" + strId);

        }
    }
}
